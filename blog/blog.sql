-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 17 2016 г., 23:33
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `title`, `text`, `date`) VALUES
(1, 'Saint Patrick''s Day', 'Saint Patrick''s Day was made an official Christian feast day in the early 17th century and is observed by the Catholic Church, the Anglican Communion (especially the Church of Ireland),[4] the Eastern Orthodox Church, and the Lutheran Church. The day commemorates Saint Patrick and the arrival of Christianity in Ireland,[3] and celebrates the heritage and culture of the Irish in general.[5] Celebrations generally involve public parades and festivals, céilithe, and the wearing of green attire or shamrocks.[6] Christians also attend church services[5][7] and the Lenten restrictions on eating and drinking alcohol are lifted for the day, which has encouraged and propagated the holiday''s tradition of alcohol consumption', '2016-03-17 06:38:06'),
(3, 'Bayerische Motoren Werke', 'BMW was established as a business entity following a restructuring of the Rapp Motorenwerke aircraft manufacturing firm in 1917. After the end of World War I in 1918, BMW was forced to cease aircraft-engine production by the terms of the Versailles Armistice Treaty.[5] The company consequently shifted to motorcycle production as the restrictions of the treaty started to be lifted in 1923,[6] followed by automobiles in 1928–29.', '2016-03-17 02:14:19'),
(4, 'Deutsche Welle', 'Deutsche Welle (German pronunciation: [ˈdɔʏtʃə ˈvɛlə]; "German Wave" in German) or DW is Germany''s international broadcaster. The service is aimed towards audiences outside of Germany and is available via television, radio and the Internet. DW Radio broadcasts news and information in thirty languages and the satellite television service consists of channels in English, German, Spanish and Arabic.', '2016-03-14 10:07:00');

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `email` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `article_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=57 ;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `text`, `email`, `date`, `article_id`) VALUES
(47, 'Lenovo has operations in more than 60 countries and sells its products', 'mamail@g.com', '2016-03-17 21:10:49', 1),
(48, 'and the Lutheran Church. The day commemorates Saint Patrick and the arrival of Christianity', 'greg@mail.ru', '2016-03-17 21:11:23', 1),
(49, '&lt;h4&gt;Add a comment&lt;h4&gt;', 'Example@mail.ua', '2016-03-17 21:11:46', 1),
(52, 'was established as a business entity following a restructuring of the Rapp Motorenwerke aircraft manufacturing firm', 'split@mail.ua', '2016-03-17 21:15:33', 3),
(55, 'satellite television service consists of channels in English, German, Spanish and Arabic.', 'tram@gmail.com', '2016-03-17 21:17:08', 4),
(56, 'English, German, Spanish and Arabic.', 'soul@mail.my', '2016-03-17 21:17:46', 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
