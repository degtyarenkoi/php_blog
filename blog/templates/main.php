﻿<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>article</title>
<link rel="stylesheet" type="text/css" href="%root%/reset.css" />
<link rel="stylesheet" type="text/css" href="%root%/head.css" />
<link rel="stylesheet" type="text/css" href="%root%/content.css" />
</head>

<body>
	<div id="wrap">
		<div id="head">
			<ul>
				<li><a href="/blog/index.php">Home</a></li>
				<li><a href="#">Contact</a></li>
				<li><a href="#">About us</a></li>
				<li><a href="#">Join</a></li>
			</ul>
		</div>
		<div id="content">
			%content%
		</div>
	</div>
</body>
</html>
