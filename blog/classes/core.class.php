<?
	abstract class Core {
	
		public static $p;
		
		public function getBody(){
			$path = ROOT."/templates/main.php";
			if(file_exists($path)){
				$tmp = file_get_contents($path);
				$tmp = $this -> replaceStringContent($tmp);
				$tmp = $this -> replaceStringRoot($tmp);
				echo $tmp;
			}
		}
		
		public function replaceStringRoot($tmp){
			return str_replace("%root%", "css", $tmp);
		}
		
		public function replaceStringContent($tmp){
			return str_replace("%content%", $this->getContent(), $tmp);
		}
		
		public function getView(){
			$path = ROOT."/templates/".self::$p;
			
			$args = func_get_args();
			$list = array();
			
			for($i=0; $i<count($args); $i++){
				if(!($i%2)){
					$list[$args[$i]] = $args[$i+1];
				}
			}
			
			if(file_exists($path)){
				$tmp = file_get_contents($path);
				foreach($list as $key => $value){
					$tmp = str_replace("%".$key."%", $value, $tmp);
				}
				return $tmp;
			}
		}
		
		public function clearString($str) {
			return trim(htmlspecialchars(stripslashes($str)));
		}
		
		abstract function getContent();
		
	}



?>